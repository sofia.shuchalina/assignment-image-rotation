#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image generate_image(const uint64_t width, const uint64_t height);

void delete_image(struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
