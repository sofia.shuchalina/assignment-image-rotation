#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>

unsigned char get_padding(uint64_t width) {
    return (unsigned char) (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status read_header(FILE *const in, struct bmp_header *h) {
    if (fread(h, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (h->bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (h->biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    fseek(in, h->bOffBits, SEEK_SET);

    return READ_OK;
}

enum read_status from_bmp(FILE *const in, struct image *img) {
    struct bmp_header h;
    enum read_status RS = read_header(in, &h);
    if (RS != READ_OK) {
        return RS;
    }

    struct image temp_image = generate_image(h.biWidth, h.biHeight);

    if (temp_image.data == NULL) {
        delete_image(temp_image);
        return READ_MEMORY_ERROR;
    }

    const unsigned char PADDING = get_padding(temp_image.width);

    for (size_t i = 0; i < h.biHeight; i += 1) {
        if (fread(&temp_image.data[i * temp_image.width], sizeof(struct pixel), temp_image.width, in) != temp_image.width) {
            delete_image(temp_image);
            return READ_INVALID_FILE;
        }

        if (PADDING)
            if (fseek(in, PADDING, SEEK_CUR) != 0) {
                delete_image(temp_image);
                return READ_INVALID_FILE;
            }
    }

    *img = temp_image;
    return READ_OK;
}

struct bmp_header create_header(const uint64_t width, const uint64_t height) {
    struct bmp_header h;
    h.bfType = 0x4D42;
    h.bfileSize = sizeof(struct bmp_header) + width * height * sizeof(struct pixel) + height * get_padding(width);
    h.bfReserved = 0;
    h.bOffBits = sizeof(struct bmp_header);
    h.biSize = 40;
    h.biWidth = width;
    h.biHeight = height;
    h.biPlanes = 1;
    h.biBitCount = 24;
    h.biCompression = 0;
    h.biSizeImage = 0;
    h.biXPelsPerMeter = 0;
    h.biYPelsPerMeter = 0;
    h.biClrUsed = 0;
    h.biClrImportant = 0;

    return h;
}

enum write_status to_bmp(FILE *const out, struct image const *img) {
    struct bmp_header HEADER = create_header(img->width, img->height);

    if (fwrite(&HEADER, sizeof(HEADER), 1, out) != 1) {
        return WRITE_ERROR;
    }

    const unsigned char PADDING = get_padding(img->width);

    for (size_t i = 0; i < img->height; i += 1) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }

        if (PADDING)
            if (fwrite(img->data, PADDING, 1, out) != 1)
                return WRITE_ERROR;

    }

    return WRITE_OK;
}
