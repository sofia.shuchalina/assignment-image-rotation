#include "image.h"
#include "bmp.h"
#include "rotator.h"

#include <stdio.h>
#include <stdlib.h>

enum file_mode {
    READ = 0,
    WRITE = 1
};

int open_file(const char *name, enum file_mode mode, FILE **f) {
    if (name == NULL || f == NULL) {
        return 1;
    }

    *f = fopen(name, mode ? "wb" : "rb");

    if (*f == NULL) {
        fprintf(stderr, "Could not open file %s, exiting\n", name);
        return 1;
    } else {
        printf("File %s opened successfully\n", name);
    }

    return 0;
}

int close_file(FILE *f) {
    if (fclose(f) != 0) {
        fprintf(stderr, "Could not close file, exiting\n");
        return 1;
    }
    return 0;
}

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Usage: %s <input file> <output file>", argv[0]);
        return 1;
    }

    puts("Open file");
    FILE *inputFile;
    if (open_file(argv[1], READ, &inputFile)) return 1;

    struct image img;
    puts("Read file");
    enum read_status rs = from_bmp(inputFile, &img);
    if (rs != READ_OK) {
        fprintf(stderr, "Can't read file %s\nError code: %d", argv[1], rs);
        return 1;
    }
    puts("File readed successfully");

    if (close_file(inputFile)) {
        fprintf(stderr, "Can't close file %s", argv[1]);
        return 1;
    }

    puts("Rotate");
    struct image rotated_img = rotate(img);
    puts("Rotated");

    puts("Save file");
    FILE *file_to_write;
    if (open_file(argv[2], WRITE, &file_to_write)) return 1;
    enum write_status ws = to_bmp(file_to_write, &rotated_img);
    if (ws != WRITE_OK) {
        fprintf(stderr, "Can't write file %s;\nError code: %d", argv[2], ws);
        return 1;
    }
    if (close_file(file_to_write)) return 1;
    puts("File saved");

    delete_image(img);
    delete_image(rotated_img);

    return 0;
}
