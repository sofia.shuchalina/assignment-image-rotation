#include "rotator.h"

#include <stdlib.h>

struct pixel get_pixel(const struct image *img, const uint64_t x, const uint64_t y) {
    return img->data[y * img->width + x];
}

void set_pixel(const struct image *img, const uint64_t x, const uint64_t y, const struct pixel p) {
    img->data[y * img->width + x] = p;
}

struct image rotate(struct image const source) {
    struct image result = generate_image(source.height, source.width);

    for (size_t i = 0; i < source.height; i++) {
        for (size_t j = 0; j < source.width; j++) {
            set_pixel(&result, source.height - i - 1, j, get_pixel(&source, j, i));
        }
    }

    return result;
}
