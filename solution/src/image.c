#include "image.h"

#include <stdlib.h>

struct image generate_image(const uint64_t width, const uint64_t height) {
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}

void delete_image(struct image img) {
    if (img.data != NULL) {
        free(img.data);
        img.data = NULL;
    }
}
